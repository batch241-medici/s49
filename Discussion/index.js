// Mock Database
// let posts = [];

// Post ID
// let count = 1;

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data));

// Reactive DOM with Fetch (CRUD Operation)

// Add Post Data

document.querySelector("#form-add-post").addEventListener('submit', (e) => {
	// Prevents the default behavior of an event
	// To prevent the page from reloading (default behavior of submit)
	e.preventDefault();

	/*posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	count++;
	console.log(posts);
	alert("Post successfully added!");

	showPost();*/

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post successfully added");
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});

// Retrieve Post
const showPosts = (posts) => {
	// variable that will contain all the posts
	let postEntries = "";
	// Looping through array items
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	console.log(postEntries);
	document.querySelector('#div-post-entries').innerHTML = postEntries
};

// Edit Post (Edit Button)
const editPost = (id) => {

	// The function first uses the querySelector() method to get the element with the id "#post-title-${id}" and "#post-body-${id}" and assigns its innerHTML property to the title variable with the same body
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	// removeAttribute() - removes the attribute with the specified name from the element
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// Update Post (Update Button)
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

/*	for(let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {

			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPost(posts);
			alert("Post successfully updated!");
		}
	}*/

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Post successfully updated!');
		// Reset the edit post form input fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		// setAttribute() - adds/sets an attribute to an HTML element
		document.querySelector('btn-submit-update').setAttribute('disabled', true);
	});
});

// Delete Post (Delete Button)
/*const deletePost = (id) => {
	document.querySelector('#div-post-entries').remove();
}*/
const deletePost = (id) => {
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE'
	})
	.then(document.querySelector(`#post-${id}`).remove())
}